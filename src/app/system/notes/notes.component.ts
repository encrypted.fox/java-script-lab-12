import {Component, OnInit} from '@angular/core';
import {UserService} from '../../shared/services/user.service';
import {AuthService} from '../../shared/services/auth.service';
import {isNullOrUndefined} from 'util';
import {Note} from 'src/app/shared/models/note.model';
import {User} from '../../shared/models/user.model';
import {NotesService} from '../../shared/services/notes.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {

  notes: Note[];
  users: User[];
  selectedUser: User;
  noteText = '';
  changeStat = false;
  selectedNote: Note;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private notesService: NotesService,
  ) {
  }

  async ngOnInit() {
    this.selectedUser = this.authService.getCurUser();
    try {
      let users = this.userService.getUsers();
      this.users = (isNullOrUndefined(await users)) ? [] : await users;
    } catch (err) {
      console.error(err);
    }

    try {
      let notes = this.notesService.getNotes();
      this.notes = (isNullOrUndefined(await notes)) ? [] : await notes;
    } catch (err) {
      console.error(err);
    }
  }

  async addNote() {
    let id = this.notes.length + 1;
    let text = this.noteText;
    let user_id = this.selectedUser.id;
    let note = {id: id, text: text, user_id: user_id, deleted: false};
    this.notesService.postNotes(note);
    this.notes = await this.notesService.getNotes();
  }

  deleteNote(note) {
    note.deleted = true;
    this.notesService.putNoteById(note.id, note);
    this.notes[this.notes.indexOf(note)].deleted = true;
  }

  async editNote(note) {
    let newNote = {id: note.id, text: this.noteText, user_id: note.user_id, deleted: note.deleted};
    this.notes[this.notes.indexOf(note)] = newNote;
    this.notesService.putNoteById(note.id, newNote);
    this.notes = await this.notesService.getNotes();
    this.changeStat = false;
  }

  changeSt(note) {
    if (this.changeStat) {
      this.changeStat = false;
      this.noteText = '';
    } else {
      this.changeStat = true;
      this.selectedNote = note;
      this.noteText = note.text;
    }
  }
}
