import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {NotesComponent} from './notes.component';

const routes: Routes = [
  {
    path: '', component: NotesComponent, children: [
      // { path: 'users', loadChildren:  './users/users.module#UsersModule'}
      // { path: 'edit: id', component:  },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class NotesRoutingModule { }
