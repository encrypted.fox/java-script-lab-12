import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SystemComponent } from './system.component';

const routes: Routes = [
  {
    path: 'system', component: SystemComponent, children: [
      // { path: 'users', loadChildren: () => UsersModule, },
      // { path: 'notes', loadChildren: () => NotesModule, }
      { path: 'users', loadChildren: './users/users.module#UsersModule' },
      { path: 'notes', loadChildren: './notes/notes.module#NotesModule' }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SystemRoutingModule { }
