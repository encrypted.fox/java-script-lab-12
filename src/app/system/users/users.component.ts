import {Component, OnInit} from '@angular/core';
import {UserService} from '../../shared/services/user.service';
import {AuthService} from '../../shared/services/auth.service';
import {NotesService} from '../../shared/services/notes.service';
import {Note} from '../../shared/models/note.model';
import {User} from '../../shared/models/user.model';
import {isNullOrUndefined} from 'util';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {

  notes: Note[];
  users: User[];
  selectedUser: User;

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private notesService: NotesService
  ) {
  }

  async ngOnInit() {
    try {
      let users = this.userService.getUsers();
      this.users = (isNullOrUndefined(await users)) ? [] : await users;
      this.selectedUser = this.authService.getCurUser();
    } catch (err) {
      console.error(err);
    }
  }

  // async addNote() {
  //   let id = this.notes.length + 1;
  //   let text = this.noteText;
  //   let user_id = this.selectedUser.id;
  //   let note = {id: id, text: text, user_id: user_id, deleted: false};
  //   this.notesService.postNotes(note);
  //   this.notes = await this.notesService.getNotes();
  // }

  deleteUser(user) {
    user.deleted = true;
    this.userService.putUserById(user.id, user);
    this.users[this.users.indexOf(user)].deleted = true;
  }

  makeAdmin(user) {

    if (!user.isadmin || isNullOrUndefined(user.isadmin)) {
      alert('Пользователь ' + user.name + ' теперь администратор!');
      user.isadmin = true;
      this.userService.putUserById(user.id, user);
    } else {
      alert('Пользователь уже был администратором!')
    }
  }
}
