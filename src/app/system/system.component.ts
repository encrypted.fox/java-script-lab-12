import { Component, OnInit } from '@angular/core';
import {AuthService} from '../shared/services/auth.service';

@Component({
  selector: 'app-system',
  templateUrl: './system.component.html',
  styleUrls: ['./system.component.css']
})
export class SystemComponent implements OnInit {

  selectedUser;

  constructor(private authService: AuthService) { }

  ngOnInit() {
    this.selectedUser = this.authService.getCurUser();
  }

}
