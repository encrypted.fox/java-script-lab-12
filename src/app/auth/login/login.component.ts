import {Component, OnInit} from '@angular/core';
import {UserService} from 'src/app/shared/services/user.service';
import {User} from 'src/app/shared/models/user.model';
import {isNullOrUndefined} from 'util';
import {AuthService} from 'src/app/shared/services/auth.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  users: User[];
  selectedUser: '';

  constructor(
    private userService: UserService,
    private authService: AuthService,
    private router: Router
  ) {
  }

  async ngOnInit() {
    try {
      let users = this.userService.getUsers();
      this.users = (isNullOrUndefined(await users)) ? [] : await users;
      this.users = this.users.filter((user) => !user.deleted);
    } catch (err) {
      console.error(err);
    }
  }

  match() {
    for (let i = 0; i < this.users.length; i++) {
      if (this.users[i].id.toString() === this.selectedUser) {
        return this.users[i];
      }
    }
  }

  onSelect() {
    let user = this.match();

    this.authService.login(user);
    this.router.navigate(['/system/notes']);
  }

}
